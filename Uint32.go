package asafe

import "sync/atomic"

type Uint32 struct {
	num uint32
}

func (s *Uint32) Set(n uint32) {
	atomic.StoreUint32(&s.num, n)
}
func (s *Uint32) ComapareAndSet(cond, n uint32) bool {
	return atomic.CompareAndSwapUint32(&s.num, cond, n)
}
func (s *Uint32) Get() uint32 {
	return atomic.LoadUint32(&s.num)
}
func (s *Uint32) Add(delta uint32) uint32 {
	return atomic.AddUint32(&s.num, delta)
}
func (s *Uint32) Compare(another uint32) CompareResult {
	if s.Get() > another {
		return Higher
	}
	if s.Get() < another {
		return Lower
	}
	return Equal
}
func (s *Uint32) ComapreAndAdd(cond, delta uint32) bool {
	return atomic.CompareAndSwapUint32(&s.num, cond, s.num+delta)
}
