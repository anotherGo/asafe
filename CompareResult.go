package asafe

type CompareResult uint8

const (
	Equal     CompareResult = 0
	Higher    CompareResult = 1
	Lower     CompareResult = 2
	Same      CompareResult = 3
	Different CompareResult = 4
	Before    CompareResult = Lower
	After     CompareResult = Higher
	Older     CompareResult = Lower
	Newer     CompareResult = Higher
)
