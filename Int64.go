package asafe

import "sync/atomic"

type Int64 struct {
	num int64
}

func (s *Int64) Set(n int64) {
	atomic.StoreInt64(&s.num, n)
}
func (s *Int64) ComapareAndSet(cond, n int64) bool {
	return atomic.CompareAndSwapInt64(&s.num, cond, n)
}
func (s *Int64) Get() int64 {
	return atomic.LoadInt64(&s.num)
}
func (s *Int64) Add(delta int64) int64 {
	return atomic.AddInt64(&s.num, delta)
}
func (s *Int64) Compare(another int64) CompareResult {
	if s.Get() > another {
		return Higher
	}
	if s.Get() < another {
		return Lower
	}
	return Equal
}
func (s *Int64) ComapreAndAdd(cond, delta int64) bool {
	return atomic.CompareAndSwapInt64(&s.num, cond, s.num+delta)
}
