package asafe

import (
	"reflect"
	"sync/atomic"
	"unsafe"
)

type Ptr struct {
	ptr unsafe.Pointer
}

func (s *Ptr) Set(n unsafe.Pointer) {
	atomic.StorePointer(&s.ptr, n)
}
func (s *Ptr) ComapareAndSet(cond, n unsafe.Pointer) bool {
	return atomic.CompareAndSwapPointer(&s.ptr, cond, n)
}
func (s *Ptr) Get() unsafe.Pointer {
	return atomic.LoadPointer(&s.ptr)
}
func (s *Ptr) Compare(another unsafe.Pointer) CompareResult {
	if uintptr(s.Get()) == uintptr(another) {
		return Equal
	}
	if reflect.DeepEqual((interface{})(another), (interface{})(s.ptr)) {
		return Equal
	}
	return Different
}
